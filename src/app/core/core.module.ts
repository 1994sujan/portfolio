import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphCardComponent } from './graph-card/graph-card.component';
import { LeftMenuNavigationComponent } from './left-menu-navigation/left-menu-navigation.component';
import { ImageCardComponent } from './image-card/image-card.component';

// MDB Angular Pro
import { ButtonsModule, WavesModule, IconsModule, ModalModule, TooltipModule } from 'angular-bootstrap-md';
import { RouterModule } from '@angular/router';
import { PageTitleComponent } from './page-title/page-title.component';
import { ListComponent } from './list/list.component';

@NgModule({
  declarations: [
    GraphCardComponent,
    LeftMenuNavigationComponent,
    ImageCardComponent,
    PageTitleComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    ButtonsModule,
    WavesModule,
    ButtonsModule,
    IconsModule,
    ModalModule.forRoot(),
    TooltipModule,
    RouterModule
  ],
  exports: [
    GraphCardComponent,
    LeftMenuNavigationComponent,
    ImageCardComponent,
    PageTitleComponent,
    ListComponent
  ]
})
export class CoreModule { }
