import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-graph-card',
  templateUrl: './graph-card.component.html',
  styleUrls: ['./graph-card.component.scss']
})
export class GraphCardComponent implements OnInit {

  @Input() title = '';

  constructor() { }

  ngOnInit() {
  }

}
