import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftMenuNavigationComponent } from './left-menu-navigation.component';

describe('LeftMenuNavigationComponent', () => {
  let component: LeftMenuNavigationComponent;
  let fixture: ComponentFixture<LeftMenuNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftMenuNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftMenuNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
