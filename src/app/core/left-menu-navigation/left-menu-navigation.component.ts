import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-left-menu-navigation',
  templateUrl: './left-menu-navigation.component.html',
  styleUrls: ['./left-menu-navigation.component.scss']
})
export class LeftMenuNavigationComponent implements OnInit {

  menuOptions = [
    {
    title: 'CUE Today',
    icon: 'home',
    link : '/app/cue-today'
  },
  {
    title: 'Dashboard',
    icon: 'tachometer-alt',
    link : '/app/dashboard'
  },
  {
    title: 'ANALYTICS (PRINTING)',
    icon: 'chart-area',
    link : '/app/analytics'
  },
  {
    title: 'Pulse & Alerting',
    icon: 'poll',
    link : '/app/pulse-alerting'
  },
  {
    title: 'User Management',
    icon: 'users-cog',
    link : '/app/user-management'
  },
  {
    title: 'Settings',
    icon: 'cog',
    link : '/app/settings'
  }
];

selectedMenu = '';

  @Input() leftMenuToggle = false;

  constructor() { }

  ngOnInit() {
  }

  openSubMenu(target: string) {
    // this.leftMenuToggle = true;
    if (this.selectedMenu  === target ) {
      this.selectedMenu = '';
      return;
    }
    this.selectedMenu = target;
  }

}
