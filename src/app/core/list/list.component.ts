import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() item = {};
  @Input() options = {};


  // tslint:disable-next-line: no-output-on-prefix
  @Output() onItemAdd: EventEmitter<any> = new EventEmitter<any>();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onItemDelete: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  onAdd(item) {
    this.onItemAdd.emit(item);
  }

  onDelete(item) {
    this.onItemDelete.emit(item);
  }



}
