import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { CoreModule } from '../core/core.module';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { AngularToastifyModule } from 'angular-toastify';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
    VirtualScrollerModule,
    CoreModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
