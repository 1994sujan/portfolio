import { Component, OnInit } from '@angular/core';
import _ from 'lodash';
import { CueService } from '../utils/cue.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  listItems = [];

  rightPanelListItems = [];

  itemcount = 0;

  leftPanelListOptions = {
    isDeletedDisable : true
  };

  rightPanelListOptions = {
    isAddDisable: true
  };

  constructor(
    private cueService: CueService
  ) { }

  ngOnInit() {

    for (let i = 0; i < 1000; i++) {
      this.listItems.push({
        id: i,
        title : `List Tile - ${i}`,
        description: `List Description - ${i}`
      });
    }
  }

  onItemAdded($event) {
    const check = _.findIndex(this.rightPanelListItems, (rItem) => {
      return rItem.id === $event.id;
    });

    if (check === -1) {
    this.itemcount++;
    this.cueService.setCurrentItemCount(this.itemcount);
    this.rightPanelListItems.push($event);
    } else {
      alert('This item is already Aded...');
    }
  }

  onItemDeleted($event) {
    _.remove(this.rightPanelListItems, (item) => {
        return item.id === $event.id ;
    });
    this.itemcount--;
    this.cueService.setCurrentItemCount(this.itemcount);
    alert(`Selected Item is deleted Scu....${this.itemcount}`);
  }
}
