import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CueTodayRoutingModule } from './cue-today-routing.module';
import { CueTodayComponent } from './cue-today.component';
import { CoreModule } from '../core/core.module';


@NgModule({
  declarations: [CueTodayComponent],
  imports: [
    CommonModule,
    CoreModule,
    CueTodayRoutingModule
  ]
})
export class CueTodayModule { }
