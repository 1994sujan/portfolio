import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CueTodayComponent } from './cue-today.component';

describe('CueTodayComponent', () => {
  let component: CueTodayComponent;
  let fixture: ComponentFixture<CueTodayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CueTodayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CueTodayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
