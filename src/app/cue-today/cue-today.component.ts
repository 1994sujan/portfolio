import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CueService } from '../utils/cue.service';

@Component({
  selector: 'app-cue-today',
  templateUrl: './cue-today.component.html',
  styleUrls: ['./cue-today.component.scss']
})
export class CueTodayComponent implements OnInit {
  items = []; 
  title ='CUE Today';
  constructor(
    private router: Router,
    private cueService: CueService
  ) { }

  ngOnInit() {
    for (let i = 0; i < 50; i++) {
      this.items.push({
        id: i,
        title: `Card-${i}`,
        description: `Description - ${i * 10}`
      });
    }
  }

  onDetails(item) {
    this.cueService.setSelectedCard(item);
    this.router.navigate(['/app/analytics']);
  }

}
