import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CueTodayComponent } from './cue-today.component';


const routes: Routes = [
  {
    path: '', component: CueTodayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CueTodayRoutingModule { }
