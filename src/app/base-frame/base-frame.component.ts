import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserInformationService } from '../utils/user.information.service';
import { CueService } from '../utils/cue.service';

@Component({
  selector: 'app-base-frame',
  templateUrl: './base-frame.component.html',
  styleUrls: ['./base-frame.component.scss']
})
export class BaseFrameComponent implements OnInit, OnDestroy {
  leftMenuToggle = false;
  userActivated = false;
  userInformation = {
    username: ''
  };
  Itemnumber = 0;

  constructor(
    private userInformationService: UserInformationService,
    private cueService: CueService
  ) {
    this.userInformation = this.userInformationService.getUserInformation() || {};
    this.userActivated = true;
    
  }

  ngOnInit() {
   this.cueService.getCurrentItemCount().subscribe(
     (val)=>{
       console.log(val);
       this.Itemnumber = val;
       
     }
   );
    
  }

  onToggleMenu() {
    this.leftMenuToggle = !this.leftMenuToggle;
   /*  if (!this.leftMenuToggle) {
    this.selectedMenu = '';
    } */
  }

  onSuccess($event) {
    console.log($event);
    this.userInformation = $event || {};
    this.userActivated = true;
  }

  ngOnDestroy() {
   this.userActivated = false;
  }

}
