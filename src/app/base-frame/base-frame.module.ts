import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseFrameComponent } from './base-frame.component';
import { RouterModule } from '@angular/router';
// For MDB Angular Free
import { NavbarModule, WavesModule, ButtonsModule, IconsModule, ModalModule, TooltipModule, PopoverModule } from 'angular-bootstrap-md';
import { LoginModule } from '../login/login.module';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [BaseFrameComponent],
  imports: [
    CommonModule,
    RouterModule,
    CoreModule,
    LoginModule,
    NavbarModule,
    WavesModule,
    ButtonsModule,
    IconsModule,
    ModalModule.forRoot(),
    TooltipModule
  ],
  exports: [BaseFrameComponent],
  providers : [PopoverModule]
})
export class BaseFrameModule { }
