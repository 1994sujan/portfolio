import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './login/login.module';
import { BaseFrameModule } from './base-frame/base-frame.module';
import { CoreModule } from './core/core.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CueTodayModule } from './cue-today/cue-today.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { AnalyticsModule } from './analytics/analytics.module';
import { UserInformationService } from './utils/user.information.service';
import { CueService } from './utils/cue.service';
import { ToastService, AngularToastifyModule } from 'angular-toastify';
import { PulseAlertingModule } from './pulse-alerting/pulse-alerting.module';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule,
    AppRoutingModule,
    AngularToastifyModule,
    LoginModule,
    BaseFrameModule,
    DashboardModule,
    AnalyticsModule,
    CoreModule,
    CueTodayModule,
    PulseAlertingModule
  ],
  providers: [
    UserInformationService,
    CueService,
    ToastService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
