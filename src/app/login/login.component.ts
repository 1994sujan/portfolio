import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserInformationService } from '../utils/user.information.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  // tslint:disable-next-line: no-output-on-prefix
  @Output() onSuccess: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private userInformationService: UserInformationService
  ) { }

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  }

  get f() { return this.loginForm.controls; }


  onLogin() {

    // Serve Call
    this.userInformationService.setUserInformation({
      username: this.f.username.value,
      password: this.f.password.value
    });

    /* this.onSuccess.emit({
      username: this.f.username.value,
      password: this.f.password.value
    }); */

    this.router.navigate(['app/dashboard']);
  }
}
