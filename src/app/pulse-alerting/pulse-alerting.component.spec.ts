import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PulseAlertingComponent } from './pulse-alerting.component';

describe('PulseAlertingComponent', () => {
  let component: PulseAlertingComponent;
  let fixture: ComponentFixture<PulseAlertingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PulseAlertingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PulseAlertingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
