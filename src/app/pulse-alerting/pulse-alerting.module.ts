import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PulseAlertingRoutingModule } from './pulse-alerting-routing.module';
import { PulseAlertingComponent } from './pulse-alerting.component';


@NgModule({
  declarations: [PulseAlertingComponent],
  imports: [
    CommonModule,
    PulseAlertingRoutingModule
  ]
})
export class PulseAlertingModule { }
