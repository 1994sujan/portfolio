import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PulseAlertingComponent } from './pulse-alerting.component';


const routes: Routes = [
  {
    path :'',
    component: PulseAlertingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PulseAlertingRoutingModule { }
