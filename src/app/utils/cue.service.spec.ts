import { TestBed } from '@angular/core/testing';

import { CueService } from './cue.service';

describe('CueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CueService = TestBed.get(CueService);
    expect(service).toBeTruthy();
  });
});
