import { Injectable } from '@angular/core';

@Injectable()
export class UserInformationService {

    private userInformation;

    setUserInformation(val) {
        this.userInformation = val;
    }

    getUserInformation() {
        return this.userInformation;
    }
}
