import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CueService {

  private selectedCardInfo;
  private currentItemcount= new Subject<number>();;

  constructor() { }

  setSelectedCard( val ) {
    this.selectedCardInfo = val;
  }

  getSelectedCard() {
    return this.selectedCardInfo;
  }

  setCurrentItemCount(count){
    this.currentItemcount.next(count);
  }

  getCurrentItemCount(){
    return this.currentItemcount.asObservable();
  }
}
