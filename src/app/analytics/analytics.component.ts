import { Component, OnInit, OnDestroy } from '@angular/core';
import { CueService } from '../utils/cue.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit, OnDestroy {

  selectedCardInfo;

  selected = false;

  type = 'line';
data = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'My First dataset',
      data: [65, 59, 80, 81, 56, 55, 40],
      backgroundColor: '#FFFFFF'
    }
  ]
};
options = {
  responsive: true,
  maintainAspectRatio: false
};


  constructor(
    private cueService: CueService
  ) { }

  ngOnInit() {
    this.selectedCardInfo = this.cueService.getSelectedCard() || {};
    if(this.selectedCardInfo.id !== undefined) {
      this.selected = true;
    }
  }

  ngOnDestroy() {
    this.cueService.setSelectedCard(undefined);
  }
}
