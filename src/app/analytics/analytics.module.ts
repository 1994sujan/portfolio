import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnalyticsRoutingModule } from './analytics-routing.module';
import { AnalyticsComponent } from './analytics.component';
import { CoreModule } from '../core/core.module';
import { ChartModule } from 'angular2-chartjs';

@NgModule({
  declarations: [
    AnalyticsComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    ChartModule,
    AnalyticsRoutingModule
  ]
})
export class AnalyticsModule { }
