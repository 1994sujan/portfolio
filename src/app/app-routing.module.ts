import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseFrameComponent } from './base-frame/base-frame.component';


const routes: Routes = [
  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule)},
  {path: 'app',
    component: BaseFrameComponent, children: [
      {path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)},
      {path: 'analytics', loadChildren: () => import('./analytics/analytics.module').then(m => m.AnalyticsModule)},
      {path: 'cue-today', loadChildren: () => import('./cue-today/cue-today.module').then(m => m.CueTodayModule)},
      {path: 'pulse-alerting', loadChildren: () => import('./pulse-alerting/pulse-alerting.module').then(m => m.PulseAlertingModule)}
    ]
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
